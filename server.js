var express = require('express')
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var guestBook = [];
var host = "";
var playlist = [];
var currentVideoId = "";


http.listen(process.env.PORT || 3000, function() {
   console.log(`listening on *:${process.env.PORT || 3000}`);
});

app.use(express.static('styles')); // serving static files
app.use(express.static('js')); // serving static files

app.get('/', function(req, res) {
   res.sendFile(__dirname + '/play.html'); //serving html
});


io.on('connection', function(socket) { //on established connection
  //transfer timecode data
  //also do like an open host setting
  var user = assignUser(socket);

  if (!user.host) { // ask host for current player state
    io.to(host).emit('request-player-status', socket.id);
  }

  socket.on('request-player-status', (status) => {
    console.log('user', status.user.id, 'requested sync');
    if (guestBook.filter((u) => (u.id == socket.id)).length == 1) {
      io.to(host).emit('request-player-status', socket.id);
    }
  })

  socket.on('provide-player-status', (status) => {
    console.log('host provided player status to user', status.id);
    if (socket.id == host) {
      console.log('updating user', status.id);
      status.videoId = getVideoId(status.url)
      io.to(`${status.id}`).emit('sync-player', status);
    }
  })

  socket.on('url-submitted', (url) => {
    let user = guestBook.filter((u) => (u.id == socket.id))[0];
    if (user.host) {
      console.log('host submitted video', getVideoId(url));
      //update playlist
      playlist.push(getVideoId(url))
      //emit playlist to all users
      console.log('emitting playlist', playlist);
      io.emit('sync-playlist', playlist)
    }
  });

  socket.on('change-state', (status) => {
    // state is an object of player state and time
    let user = guestBook.filter((u) => (u.id == socket.id))[0];

    status['user'] = user;
    status.url = getVideoId(status.url)

    if (user.host) { //if user is a host
      console.log(`host changed player to state ${status.state} at time ${status.time}`);
      socket.broadcast.emit('state-changed', status);
    } else if (user) { // as long as user is in guestBook
      console.log(`user ${status.user} changed player to state ${status.state} at time ${status.time}`);
      // socket.broadcast.emit('state-changed', status);
    }
  });

  socket.on('emoji', (emoji) => {
    io.emit('emoji', emoji);
  })

  socket.on('give-host', (id) => {
    let oldHost = guestBook.filter((u) => (u.id == socket.id))[0];

    if (oldHost.host && oldHost.id != id) {
      console.log('granting host to', id);

      let newHost = guestBook.filter((u) => (u.id == id))[0];

      oldHost.host = false;
      newHost.host = true;

      socket.emit('assigned-user', oldHost);
      io.to(id).emit('assigned-user', newHost);
      io.emit('sync-guestBook', guestBook);
    }
  });

  socket.on('disconnect', () => {
    disconnect(socket)
  });

  socket.on('modCmd', (command) => {
    console.log('mod command recieved: ', command);
    if (command == 'wipeguests') { // reload all current guests
      socket.broadcast.emit('modCmd', 'reload');
    } else if (command == 'yankhost') { //steal host
      let oldHost = guestBook.filter((u) => u.host)[0];
      let newHost = guestBook.filter((u) => u.id == socket.id)[0];

      oldHost.host = false;
      newHost.host = true;

      socket.emit('assigned-user', newHost);
      io.to(oldHost.id).emit('assigned-user', oldHost);
      io.emit('sync-guestBook', guestBook);
    }
  })
});


function assignUser(socket) {

  let user = { //assign user
    id: socket.id,
    host: (guestBook.length == 0),
    color: getRandomColor(),
    emoji: getRandomEmoji(),
    status: {}
  };

  if (user.host) {host = user.id}

  guestBook.push(user);
  console.log(`A user connected with id ${socket.id}, ${guestBook.length} connected on IP ${socket.handshake.address}`);

  socket.emit('assigned-user', user);
  socket.emit('sync-playlist', playlist)
  io.emit('sync-guestBook', guestBook);

  return user;
}

function disconnect(socket) {
  guestBook = guestBook.filter((u) => u.id !== socket.id)

  if (socket.id == host && guestBook.length>0) {
    console.log('host left')
    let newHost = guestBook[0];
    newHost.host = true;
    host = newHost.id;
    io.to(`${host}`).emit('assigned-user', newHost);
  }
  console.log(`A user disconnected with id ${socket.id}, ${guestBook.length} connected`);
  io.emit('sync-guestBook', guestBook);
}

function getVideoId(url) {
  var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
  var match = url.match(regExp);

  if (match && match[2].length == 11) {
    return match[2];
  } else {
    return 'error';
  }
}

function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function getRandomEmoji() {
  var emojis = [
    128513, 128514, 128515, 128516, 128517, 128518, 128521, 128522, 128523,
    128524, 128525, 128527, 128530, 128531, 128532, 128534, 128536, 128538,
    128540, 128541, 128542, 128544, 128545, 128546, 128547, 128548, 128549,
    128552, 128553, 128554, 128555, 128557, 128560, 128561, 128562, 128563,
    128565, 128567, 129312, 128012, 128013, 128014, 128017, 128018, 128020,
    128023, 128024, 128025, 128026, 128027, 128028, 128029, 128030, 128031,
    128032, 128033, 128034, 128035, 128036, 128037, 128038, 128039, 128040,
    128043, 128044, 128045, 128046, 128047, 128048, 128049, 128050, 128051,
    128052, 128053, 128054, 128055, 128056, 128057, 128058, 128059, 128060,
    127813, 127814, 127815, 127816, 127817, 127818, 127820, 127821, 127822,
    127823, 127825, 127826, 127827, 127828, 127829, 127830, 127831, 127832,
    127837, 127838, 127839, 127840, 127841, 127842, 127843, 127844, 127846,
    127848, 127849, 127850, 127851, 127852, 127853, 127856, 127858, 127859,
    127860, 127863, 127864, 127867, 127921, 127923, 127934, 127935, 127936,
    127937, 127938, 127939, 127940, 127942, 127944, 127946, 128675, 128692,
    128693, 9917, 9918, 9978, 127907, 127919, 9971, 127890, 127880, 127881,
    127887, 127891, 127905, 127906, 127908, 127909, 127911, 127912, 127915,
    127916, 127918, 127919, 127926, 127927, 127928, 127929, 127930, 127931,
    127932, 127968, 127973, 127978, 128147, 128148, 128149, 128150, 128151,
    128152, 128187, 128186, 128197, 128213, 128247, 128641, 128642, 128646,
    128648, 128650, 128653, 128654, 128656, 128660, 128662, 128664, 128667,
    128668, 128669, 128670, 128671, 128672, 128673, 128640, 128643, 128644,
    128645, 128647, 128649, 128652, 128657, 128658, 128659, 128661, 128663,
    128665, 128666, 128674, 128676, 128690
  ];
  return emojis[Math.floor(Math.random()*emojis.length)].toString();
}
