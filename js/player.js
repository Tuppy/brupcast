//https://developers.google.com/youtube/iframe_api_reference

// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.

var player;

function onYouTubeIframeAPIReady() {
  player = new YT.Player('player', {
    height: '70%',
    width: '100%',
    videoId: 'zyY7NU4cbtY',
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange
    }
  });
}

function playVideo(id) {
  function waitForPlayer(){
    // check to see if player is initialized every .5 seconds.
    // dont like this at all
    if(typeof player !== "undefined" && player.getPlayerState){

      player.loadVideoById({
        videoId: id,
      });

      currentVideoId = id;
    }
    else{
      setTimeout(waitForPlayer, 500);
    }
  }
  waitForPlayer();
}

function syncPlayer(status) {
  function waitForPlayer(){
    // check to see if player is initialized every .5 seconds.
    // dont like this at all
    if(typeof player !== "undefined" && player.getPlayerState){

      player.loadVideoById({
        videoId: status.videoId,
        startSeconds: status.pos
      });
      changePlayerState(status.state);

      currentVideoId = status.videoId;
    }
    else{
      setTimeout(waitForPlayer, 500);
    }
  }
  waitForPlayer();
}

function onPlayerReady(event) {
  player.mute();
  if (currentVideoId) {
    console.log('fuck', currentVideoId);
    player.loadVideoById({videoId: currentVideoId}).playVideo()
  } else {
    player.stopVideo();
  }
}

function onPlayerStateChange(event) {
  socket.emit('change-state', {
    state: event.data,
    time: player.getCurrentTime(),
    url: player.getVideoUrl()
  });
}

function changePlayerState(state) {
  console.log(state)
  //manip player
  player.playVideo();
  if (state == 1) {
    console.log(`changing player state to ${state}`)
    player.playVideo()
  } else if (state == 2) {
    console.log(`changing player state to ${state}`)
    player.pauseVideo()
  } else {
    player.pauseVideo();
  }
}
